package com.example.kubik.validations

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import kotlinx.android.synthetic.main.activity_main.*
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this@MainActivity.validateUserName()
        this@MainActivity.validateEmail()
        this@MainActivity.validatePhoneNumber()
        this@MainActivity.validateRegExp()

        userNameTIET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                this@MainActivity.validateUserName()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        userEmailTIET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                this@MainActivity.validateEmail()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        userPhoneNumberTIET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                this@MainActivity.validatePhoneNumber()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        customRegExpTIET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                this@MainActivity.validateRegExp()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun validateUserName() {
        val userName = userNameTIET.text.toString()
        val validationResult: ValidationResult = ValidationType.USER_NAME__VALIDATION.runValidation(userName, false)

        userNameTIL.error = when (validationResult) {
            ValidationResult.OK -> null
            ValidationResult.TOO_SHORT -> "user name is too short, min length is ${BuildConfig.MIN_USER_NAME_LENGTH} characters"
            ValidationResult.EMPTY -> "user name is empty"
            else -> "other validation error"
        }
    }

    private fun validateEmail() {
        val userEmail = userEmailTIET.text.toString()
        val validationResult: ValidationResult = ValidationType.EMAIL__VALIDATION.runValidation(userEmail, false)

        userEmailTIL.error = when (validationResult) {
            ValidationResult.OK -> null
            ValidationResult.ERROR -> "invalid email format"
            ValidationResult.EMPTY -> "email is empty"
            else -> "other validation error"
        }
    }

    private fun validatePhoneNumber() {
        val userPhoneNumber = userPhoneNumberTIET.text.toString()
        val validationResult: ValidationResult = ValidationType.PHONE_NUMBER__VALIDATION.runValidation(userPhoneNumber, true)

        userPhoneNumberTIL.error = when (validationResult) {
            ValidationResult.OK -> null
            ValidationResult.ERROR -> "invalid phone number format"
            ValidationResult.EMPTY -> "phone number is empty"
            else -> "other validation error"
        }
    }

    private fun validateRegExp() {
        val regExpValue = customRegExpTIET.text.toString()
        val validationResult: ValidationResult = ValidationType.CUSTOM_REGEX_VALIDATION.runValidation(regExpValue, true)

        customRegExpTIL.error = when (validationResult) {
            ValidationResult.OK -> null
            ValidationResult.ERROR -> "reg exp does not match ${BuildConfig.REGEX_PATTERN}"
            ValidationResult.EMPTY -> "reg exp is empty"
            else -> "other validation error"
        }
    }

    enum class ValidationType {

        USER_NAME__VALIDATION {
            override fun runValidation(input: String, isMandatory: Boolean): ValidationResult =
                    when {
                        isMandatory && input.isEmpty() -> ValidationResult.EMPTY
                        input.length < BuildConfig.MIN_USER_NAME_LENGTH -> ValidationResult.TOO_SHORT
                        else -> ValidationResult.OK
                    }
        },
        EMAIL__VALIDATION {
            override fun runValidation(input: String, isMandatory: Boolean) =
                    when {
                        isMandatory && input.isEmpty() -> ValidationResult.EMPTY
                        Patterns.EMAIL_ADDRESS.matcher(input).matches() -> ValidationResult.OK
                        else -> ValidationResult.ERROR
                    }
        },
        PHONE_NUMBER__VALIDATION {
            override fun runValidation(input: String, isMandatory: Boolean): ValidationResult =
                    when {
                        isMandatory && input.isEmpty() -> ValidationResult.EMPTY
                        Patterns.PHONE.matcher(input).matches() -> ValidationResult.OK
                        else -> ValidationResult.ERROR
                    }

        },
        CUSTOM_REGEX_VALIDATION {
            override fun runValidation(input: String, isMandatory: Boolean): ValidationResult {
                val pattern = Pattern.compile(BuildConfig.REGEX_PATTERN)
                val matcher = pattern.matcher(input)
                return when {
                    isMandatory && input.isEmpty() -> ValidationResult.EMPTY
                    matcher.matches() -> ValidationResult.OK
                    else -> ValidationResult.ERROR
                }
            }
        };

        abstract fun runValidation(input: String, isMandatory: Boolean = false): ValidationResult
    }

    enum class ValidationResult {
        OK, TOO_LONG, TOO_SHORT, EMPTY, ERROR
    }
}
